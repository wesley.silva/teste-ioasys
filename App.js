import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Ionicons';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import Login from './src/screens/Login';
import Home from './src/screens/Home';
import Details from './src/screens/Details';
import Search from './src/screens/Search';

import Reducers from './src/redux/reducers';

const Navigation = () => {
  // const navigation = useNavigation();

  const Stack = createStackNavigator();
  const Store = createStore(Reducers);

  return (
    <Provider store={Store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Login"
            component={Login}
          />
          <Stack.Screen
            options={({ navigation }) => ({
              headerRight: () => (
                <Icon
                  style={{ padding: 10 }}
                  name="search"
                  size={22}
                  onPress={() => navigation.navigate({ name: 'Search' })}

                />
              ),
            })}
            name="Home"
            component={Home}
          />
          <Stack.Screen name="Details" component={Details} />
          <Stack.Screen name="Search" component={Search} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default Navigation;
