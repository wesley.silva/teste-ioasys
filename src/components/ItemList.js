import React from 'react';
import { useNavigation } from '@react-navigation/native';
import styled from 'styled-components/native';

const ItemList = styled.TouchableOpacity`
  background:#fff;
  margin:2px 0px;
  padding:20px 10px;
  flex-direction:row;
  width:100%;
  box-shadow: 0px 0px 0.5px black;
`;

const Content = styled.View`
margin-left:20px;
`;

const ContentLocale = styled.View`
  flex-direction:row;
`;

const Title = styled.Text`
  font-size:22px;
  font-weight:600;
  color:#444;
`;

const Type = styled.Text`
  font-size:16px;
  color:#888;
  font-weight:300;
`;

const Locale = styled.Text`
  font-size:18px;
  margin-top:5px;
`;

const Price = styled.Text`
  font-size:22px;
  margin-top:5px;
color:#666;
font-weight:bold;
`;

const Image = styled.Image`
  width:100px;
  height:100px;
  border-radius:10px;
`;

const ItemComponent = ({ data }) => {
  const navigation = useNavigation();

  return (
    <ItemList onPress={() => navigation.navigate('Details', { id: data.id })}>
      <Image source={{ uri: `http://empresas.ioasys.com.br${data.photo}` }} />
      <Content>
        <Title>{data.enterprise_name}</Title>
        <Type>{data.enterprise_type?.enterprise_type_name}</Type>
        <ContentLocale>
          <Locale>{`${data.city} - ${data.country}`}</Locale>
        </ContentLocale>
        <Price>
          $
          {data.share_price}
        </Price>
      </Content>
    </ItemList>
  );
};

export default ItemComponent;
