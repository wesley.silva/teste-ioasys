import styled from 'styled-components/native';

export const Container = styled.View`
  padding:10px;
  flex:1;
  width:100%;
`;

export const Input = styled.TextInput`
  border:1px solid #cacaca;
  height:50px;
  font-size:18px;
  padding:0 15px;
  border-radius:5px;
  color:#333;
`;

export const Label = styled.Text`
  font-size:16px;
  color:#5a5a5a;
  margin:20px 0 5px 0;
  font-weight:bold;
`;

export const ContainerPicker = styled.View`
  border:1px solid #cacaca;
  justify-content:center;
  align-items:center;
  height:50px;
  padding:0 10px;
`;

export const Button = styled.TouchableOpacity`
  justify-content:center;
  align-items:center;
  height:50px;
`;

export const TextButton = styled.Text`
  font-size:18px;
  font-weight:bold;
  color:#0000f5;
`;

export const ContainerFlat = styled.View`
  flex:1;
  justify-content:center;
`;

export const ResultNotFound = styled.Text`
  text-align:center;
  font-size:16px;
  color:#545454;
`;

export const List = styled.FlatList`
  flex:1;
  background:red;
`;

export const Indicator = styled.ActivityIndicator``;
