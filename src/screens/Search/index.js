import React, { useState, useEffect } from 'react';
import { FlatList } from 'react-native-gesture-handler';
import RNPickerSelect from 'react-native-picker-select';
import { useSelector } from 'react-redux';
import ItemList from '../../components/ItemList';
import Api from '../../Api';

import {
  ContainerPicker,
  ResultNotFound,
  ContainerFlat,
  TextButton,
  Indicator,
  Container,
  Button,
  Input,
  Label,
} from './styles';

const Search = () => {
  const [items, setItems] = useState([]);
  const [item, setItem] = useState('');
  const [enterprise, setEnterprise] = useState('');
  const [notFound, setNotFound] = useState('');
  const [enterpriseList, setEnterpriseList] = useState([]);
  const [isIndicator, setIsIndicator] = useState(false);

  const authCredentials = useSelector((state) => state.auth);

  useEffect(() => {
    async function getEnterprises() {
      try {
        const result = await Api.get('/enterprises', {
          headers: {
            'access-token': authCredentials.accessToken,
            client: authCredentials.client,
            uid: authCredentials.uid,
          },
        });
        const array = result.data.enterprises;

        const arrayItems = array.map((i) => ({
          label: i.enterprise_type.enterprise_type_name,
          value: i.enterprise_type.id,
        }));

        const uniqueArray = [];

        arrayItems.forEach((element) => {
          let contem = false;
          uniqueArray.forEach((ele) => {
            if (element.label === ele.label) {
              contem = true;
            }
          });

          if (contem === false) {
            uniqueArray.push(element);
          }
        });

        setItems(uniqueArray);
      } catch (error) {
        alert('Erro ao listar categorias');
      }
    }

    getEnterprises();
  }, []);

  const handleSearch = async () => {
    setIsIndicator(true);

    try {
      const url = `/enterprises?enterprise_types=${item}&name=${enterprise}`;

      const result = await Api.get(url, {
        headers: {
          'access-token': authCredentials.accessToken,
          client: authCredentials.client,
          uid: authCredentials.uid,
        },
      });

      if (result.data.enterprises.length === 0) {
        setNotFound('Nenhum resultado encontrado');
        setEnterpriseList([]);
      } else {
        setEnterpriseList(result.data.enterprises);
      }
    } catch (error) {
      alert('Erro ao buscar empresa');
    }

    setIsIndicator(false);
  };

  const pickerStyle = {
    inputIOS: {
      width: '100%',
      color: '#333',
      fontSize: 18,
      paddingHorizontal: 5,
    },
    inputAndroid: {
      width: '100%',
      color: 'white',
    },
  };

  return (
    <Container>
      <Label>Nome da Empresa</Label>

      <Input
        placeholder="Busque por uma empresa."
        onChangeText={(txt) => setEnterprise(txt)}
      />

      <Label>Categoria</Label>
      <ContainerPicker>
        <RNPickerSelect
          style={pickerStyle}
          value={item}
          onValueChange={(value) => setItem(value)}
          items={items}
        />
      </ContainerPicker>
      <Button onPress={handleSearch}>

        {
        isIndicator
          ? <Indicator />
          : <TextButton>Pesquisar</TextButton>
        }

      </Button>
      <ContainerFlat>

        {
          notFound !== '' && enterpriseList.length === 0
            ? <ResultNotFound>{notFound}</ResultNotFound>
            : (
              <FlatList
                keyExtractor={(i) => i.value.toString()}
                data={enterpriseList}
                renderItem={({ item: data }) => <ItemList data={data} />}
              />
            )
        }

      </ContainerFlat>
    </Container>
  );
};

export default Search;
