import styled from 'styled-components/native';

export const Container = styled.View`
  background:#fff;
  flex:1;
`;

export const Photo = styled.Image`
  width:100%;
  height:350px;
`;

export const Title = styled.Text`
  font-size:26px;
  color:#333;
  margin-top:10px;

`;

export const Price = styled.Text`
  font-size:28px;
  color:#666;
`;

export const Content = styled.View`
  padding:10px;
  flex:1;
`;

export const ContentTop = styled.View`
  flex-direction:row;
  justify-content:space-between;
  align-items:center;
`;

export const ContentDetails = styled.View`
  flex-direction:row;
  justify-content:space-between;
  margin-top:20px;
`;

export const ContentItemDetails = styled.View`
  flex:1;
  align-items:center;
  justify-content:center;
  padding:10px;
`;

export const ItemDetails = styled.Text`
  margin-top:5px;
  color:#555;
`;

export const ContainerDescription = styled.ScrollView`
  margin:20px 0;
`;

export const Description = styled.Text`
  font-size:16px;
  color:#444;
  text-align:justify;
`;

export const Indicator = styled.ActivityIndicator``;
