import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useRoute } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  ContainerDescription,
  ContentItemDetails,
  ContentDetails,
  ItemDetails,
  Description,
  ContentTop,
  Container,
  Indicator,
  Content,
  Photo,
  Price,
  Title,
} from './styles';
import Api from '../../Api';

const Details = () => {
  const [enterprise, setEnterprise] = useState({});
  const [isIndicator, setIsIndicator] = useState(true);

  const authCredentials = useSelector((state) => state.auth);
  const route = useRoute();
  const { id } = route.params;

  useEffect(() => {
    async function getEnterprise() {
      try {
        const result = await Api.get(`/enterprises/${id}`, {
          headers: {
            'access-token': authCredentials.accessToken,
            client: authCredentials.client,
            uid: authCredentials.uid,
          },
        });
        setEnterprise(result.data.enterprise);
      } catch (error) {
        alert('Erro ao mostrar dados da empresa');
      }
    }

    getEnterprise();
    setIsIndicator(false);
  }, []);

  return (

    <Container>

      {isIndicator ? <Indicator /> : (
        <>
          <Photo source={{ uri: `http://empresas.ioasys.com.br${enterprise.photo}` }} />
          <Content>
            <ContentTop>
              <Title>{enterprise.enterprise_name}</Title>
              <Price>
                $
                {enterprise.share_price}
              </Price>
            </ContentTop>
            <ContentDetails>
              <ContentItemDetails>

                <Icon
                  color="#555"
                  name="information-circle-outline"
                  size={18}
                />

                <ItemDetails>
                  {enterprise.enterprise_type?.enterprise_type_name}
                </ItemDetails>
              </ContentItemDetails>
              <ContentItemDetails>
                <Icon
                  color="#555"
                  name="location-outline"
                  size={18}
                />
                <ItemDetails>
                  {enterprise.city}
                </ItemDetails>
              </ContentItemDetails>
              <ContentItemDetails>
                <Icon
                  color="#555"
                  name="location-sharp"
                  size={18}
                />
                <ItemDetails>
                  {enterprise.country}
                </ItemDetails>
              </ContentItemDetails>
            </ContentDetails>
            <ContainerDescription showsVerticalScrollIndicator={false}>
              <Description>
                {enterprise.description}
              </Description>
            </ContainerDescription>
          </Content>
        </>
      )}

    </Container>
  );
};
export default Details;
