import React, { useState } from 'react';
import { Platform } from 'react-native';
import { useDispatch } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useNavigation } from '@react-navigation/native';
import {
  Logo,
  Input,
  Container,
  ButtonLogin,
  ButtonLoginText,
  ActivityIndicator,
  ContainerInputMail,
  ContainerInputPassword,
} from './styles';
import api from '../../Api';

const Login = () => {
  const [showPassword, setShowPassword] = useState(true);
  const [indicator, setIndicator] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const handleLogin = async () => {
    setIndicator(true);

    try {
      const resultLogin = await api.post('users/auth/sign_in', {
        email: 'testeapple@ioasys.com.br',
        password: '12341234',
      });

      dispatch({
        type: 'SET_CREDENTIALS',
        payload: {
          accessToken: resultLogin.headers['access-token'],
          client: resultLogin.headers.client,
          uid: resultLogin.headers.uid,
        },
      });

      navigation.reset({
        routes: [{ name: 'Home' }],
      });
    } catch (error) {
      alert(`Falha ao realizar login: ${error}`);
    }
    setIndicator(false);
  };

  return (
    <Container
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
    >

      <Logo
        source={require('../../img/logo.png')}
        resizeMode="contain"
      />

      <ContainerInputMail>
        <Icon
          name="envelope-o"
          size={22}
          color="#8a8a8a"
        />
        <Input
          value={email}
          placeholder="Digite seu email"
          autoFocus
          autoCapitalize="none"
          placeholderTextColor="#6a6a6a"
          onChangeText={(txt) => setEmail(txt)}
        />
      </ContainerInputMail>
      <ContainerInputPassword>
        <Icon
          name="lock"
          size={22}
          color="#8a8a8a"
        />
        <Input
          value={password}
          placeholder="Digite sua senha"
          placeholderTextColor="#6a6a6a"
          secureTextEntry={showPassword}
          onChangeText={(txt) => setPassword(txt)}
        />

        <Icon
          onPress={() => setShowPassword(!showPassword)}
          color="#8a8a8a"
          name="circle"
          size={18}
        />

      </ContainerInputPassword>
      <ButtonLogin onPress={handleLogin} disabled={indicator}>
        {
        indicator
          ? <ActivityIndicator />
          : <ButtonLoginText>Entrar</ButtonLoginText>
        }

      </ButtonLogin>
    </Container>
  );
};
export default Login;
