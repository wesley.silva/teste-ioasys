import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  display:flex;
  justify-content:center;
  align-items:center;
`;

export const EnterprisesList = styled.FlatList`
  width:100%;
`;

export const Indicator = styled.ActivityIndicator``;
