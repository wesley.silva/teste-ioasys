import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import {
  EnterprisesList,
  Container,
  Indicator,
} from './styles';
import ItemList from '../../components/ItemList';
import Api from '../../Api';

const Home = () => {
  const [enterprises, setEnterprises] = useState([]);
  const [isIndicator, setIsIndicator] = useState(true);

  const authCredentials = useSelector((state) => state.auth);

  useEffect(() => {
    async function getEnterprises() {
      try {
        const result = await Api.get('/enterprises', {
          headers: {
            'access-token': authCredentials.accessToken,
            client: authCredentials.client,
            uid: authCredentials.uid,
          },
        });
        setEnterprises(result.data.enterprises);
      } catch (error) {
        alert(`Erro ao buscar empresas: ${error}`);
      }
    }
    getEnterprises();
    setIsIndicator(false);
  }, []);

  return (
    <Container>
      {isIndicator ? <Indicator />
        : (
          <EnterprisesList
            data={enterprises}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => <ItemList data={item} />}
          />
        )}
    </Container>
  );
};
export default Home;
